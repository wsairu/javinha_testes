import java.util.Scanner;

public class Testes {
    public static void main(String[] args) {
        int idade = 0;
        String nome = "";

        Scanner scan = new Scanner(System.in);

        System.out.println("Informe seu Nome: ");
        nome = scan.next();

        System.out.println("Ok, " + nome + ", agora sua idade:");
        idade = scan.nextInt();
        System.out.println();

        System.out.println("Olá " + nome + ", você tem " + idade + " anos de idade.");

    }
    
}